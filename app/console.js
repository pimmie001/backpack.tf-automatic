var Readline = require('readline');
var Config = require('./config.js');

var steam = require('./steamclient.js');

var input = Readline.createInterface({
	"input": process.stdin,
	"output": process.stdout
});

input.on('line', function(line) {
	var parts = line.split(' ');

	switch (parts[0]) {
		case 'identity_secret':
			if(!parts[1]) {
				console.log("Usage: identity_secret <base64 identity_secret for your account>");
				break;
			}

			var config = Config.get();
			config.steam.identitySecret = config.steam.identitySecret || {};
			config.steam.identitySecret[steam.steamID.getSteamID64()] = parts[1];
			Config.write(config);

			steam.startConfirmationChecker(10000, parts[1]);
			console.log("identity_secret saved. ALL trade confirmations will be automatically accepted.");
			break;

		case 'logout':
			var config = Config.get();
			var username = config.steam.last;
			if(config.steam.oAuthTokens && config.steam.oAuthTokens[username]) {
				delete config.steam.cookies;
				delete config.steam.oAuthTokens[username];
			}

			Config.write(config);

			console.log("Logged out successfully.");
			process.exit(0);
			break;

		case 'help':
			console.log("Commands: help, identity_secret, logout");
			break;

		default:
			console.log("Unknown command '" + parts[0] + "'");
	}
});

module.exports = input;
